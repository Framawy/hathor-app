package fun.hathor.eg.SettingsFragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.HashMap;

import fun.hathor.eg.R;
import fun.hathor.eg.Classes.GlideApp;

public class AddSponsorFragment extends Fragment {

    private EditText  sponsorTxt;
    private ImageView sponsorImg;
    private Button    done_btn;
    private Uri imgUri;
    private StorageReference ref;
    private FirebaseFirestore database =  FirebaseFirestore.getInstance();

    public AddSponsorFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_sponser, container, false);

        sponsorTxt = view.findViewById(R.id.sponsor_editText);
        sponsorImg = view.findViewById(R.id.sponsor_img);
        done_btn = view.findViewById(R.id.done_btn);

        sponsorImg.setOnClickListener(view1 -> file_grab());
        done_btn.setOnClickListener(view1 -> fileUploader(sponsorTxt.getText().toString() , imgUri) );


        return view;
    }

    private void file_grab(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent , 1); }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        final int unmaskedRequestCode = requestCode & 0x0000ffff;
        if(requestCode == 1 && unmaskedRequestCode == 1 && data != null && data.getData() != null){
            imgUri = data.getData();
            GlideApp.with(this).load(imgUri).into(sponsorImg);
        }
    }



    private void fileUploader(String s , Uri u){
        if(s!= null && u!=null){
        ref= FirebaseStorage.getInstance().getReference().child("images/" + "sponsors" + System.currentTimeMillis());
        ref.putFile(imgUri).addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                ref.getDownloadUrl().addOnSuccessListener(uri -> {
                    Uri downloadUri = uri;
                    HashMap<String, String> map = new HashMap<>();
                    map.put("sponsorName" , s);
                    map.put("sponsorImg" , downloadUri.toString());
                    String docID =  database.collection("Sponsors").document().getId();
                    database.collection("Sponsors").document(docID).set(map).addOnCompleteListener(task1 -> {
                        if(task1.isSuccessful()){
                            Toast.makeText(getActivity(), "SponsorShip added", Toast.LENGTH_SHORT).show();
                        } });

                });
            }
        });
    } else{
            Toast.makeText(getActivity(), "Error!", Toast.LENGTH_SHORT).show();}
    }

    }


