package fun.hathor.eg.SettingsFragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.bumptech.glide.request.RequestOptions;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Objects;
import fun.hathor.eg.R;
import fun.hathor.eg.SignActivity;
import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.GlideApp;

public class SettingsFragment extends Fragment implements View.OnClickListener {

    private NavController nav;
    private TextView setting_profile_btn;
    private Curr_User curr_user;
    private ImageView profile_image;
    private CardView profile_btn, sponsor_btn, logout_btn;
    public SettingsFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        curr_user = new Curr_User();
        profile_btn = view.findViewById(R.id.profile_btn);
        profile_image = view.findViewById(R.id.profile_image);
        nav = Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment);
        setting_profile_btn = view.findViewById(R.id.setting_profile_btn);
        logout_btn = view.findViewById(R.id.logout_btn);
        sponsor_btn = view.findViewById(R.id.sponsor_btn);

        setting_profile_btn.setText(curr_user.getCurr_Displayname());
        GlideApp.with(this).load(curr_user.getCurr_ImageUrl()).apply(new RequestOptions().circleCrop()).into(profile_image);

        //////////////////**BUTTONS**///////////////////////
        profile_btn.setOnClickListener(this);
        sponsor_btn.setOnClickListener(this);
        logout_btn.setOnClickListener(this );


        return view;
    }

    private void signOut(){
        FirebaseAuth.getInstance().signOut();
        startActivity(new Intent(getActivity() , SignActivity.class));
    }


    @Override
    public void onClick(View v ) {

        switch (v.getId()) {

            case R.id.profile_btn:
                nav.navigate(R.id.action_settingsFragment_to_profileFragment);
                break;

            case R.id.sponsor_btn:
                //nav.navigate(R.id.action_settingsFragment_to_addSponsorFragment);
                break;

            case R.id.logout_btn:
                signOut();
                break;

            default:
                break;
        }

    }

}
