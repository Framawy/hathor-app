package fun.hathor.eg;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fun.hathor.eg.Classes.GlideApp;

public class SignActivity extends AppCompatActivity {

    NavController sign_nav;
    ImageView back;
    TextView title;
    ImageView background;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign);
        back = findViewById(R.id.back_arrow);
        title = findViewById(R.id.toolbar_title);
        background = findViewById(R.id.signBackgorund);

        GlideApp.with(this).load(R.drawable.sign_background).into(background);

        sign_nav = Navigation.findNavController(this, R.id.sign_nav_host_fragment);
        sign_nav.addOnDestinationChangedListener((controller, destination, arguments) -> {
            switch (destination.getId()){
                case (R.id.signINFragment) :
                    back.setVisibility(View.INVISIBLE);
                    title.setText("Sign In"); break;
                case (R.id.signUPFragment) :
                    back.setVisibility(View.VISIBLE);
                    title.setText("Sign Up"); break;
            }
        });

        back.setOnClickListener(view -> sign_nav.navigateUp());

    }

    @Override
    public void onStart() {
        super.onStart();
       }
    }



