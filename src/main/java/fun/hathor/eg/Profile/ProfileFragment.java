package fun.hathor.eg.Profile;


import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.request.RequestOptions;
import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.indicator.progresspie.ProgressPieIndicator;
import com.github.piasy.biv.loader.glide.GlideImageLoader;
import com.github.piasy.biv.view.BigImageView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;
import java.util.Objects;

import fun.hathor.eg.R;
import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.GlideApp;
import fun.hathor.eg.Classes.Adapters.Posts_adapter;
import fun.hathor.eg.Classes.PostClass;
import fun.hathor.eg.Classes.ProfileViewModel.ProfileViewModel;
import fun.hathor.eg.Classes.ProfileViewModel.ProfileViewModelFactory;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Curr_User curr_user;
    private ImageView profile_image, back;
    private TextView name_txt, following_btn, followers_btn, title , followTXT;
    private StorageReference ref;
    private Uri imgUri, downloadUri;
    private RecyclerView recyclerView;
    private PostClass post;
    private NestedScrollView nestedScrollView;
    private Posts_adapter adapter;
    private NavController nav;
    private ProfileViewModel viewModel;
    private ArrayList<PostClass> list = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private ConstraintLayout loading;
    private LinearLayout follow_btn;


    public ProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BigImageViewer.initialize(GlideImageLoader.with(getContext()));
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        name_txt = view.findViewById(R.id.profile_username);
        profile_image = view.findViewById(R.id.profile_image);
        followers_btn = view.findViewById(R.id.followers_btn);
        following_btn = view.findViewById(R.id.following_btn);
        back = view.findViewById(R.id.back_arrow);
        title = view.findViewById(R.id.toolbar_title);
        nav = Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment);
        curr_user = new Curr_User();
        post = new PostClass();
        loading = view.findViewById(R.id.loadingView);
        nestedScrollView = view.findViewById(R.id.nested_scrollview);
        follow_btn = view.findViewById(R.id.follow_btn);
        followTXT = view.findViewById(R.id.follow_txt);
        followTXT.setText("Edit");
        viewModel = ViewModelProviders.of(this, new ProfileViewModelFactory
                (getActivity().getApplication(), curr_user.getCurr_ID())).get(ProfileViewModel.class);

        recyclerView = view.findViewById(R.id.recycle_profile);
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new Posts_adapter(getActivity(), list, Posts_adapter.Kind.PROFILE);
        recyclerView.setAdapter(adapter);
        follow_btn.setOnClickListener(view5 -> CropImage.activity()
                .setAspectRatio(4, 3)
                .start(getContext(), this));
        nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener)
                (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    if (scrollY == (v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight())) {
                        viewModel.adddata();
                    }
                });

        liveData();


        Uri uri = curr_user.getCurr_ImageUrl();
        GlideApp.with(this).load(uri).apply(new RequestOptions().circleCrop()).into(profile_image);

        profile_image.setOnClickListener(view1 -> {
            // custom dialog
            final Dialog dialog = new Dialog(getContext(), R.style.Custom_Dialog);
            dialog.setContentView(R.layout.image_zoom);
            // set the custom dialog components - text, image and button
            BigImageView image = dialog.findViewById(R.id.imageView_large);
            image.setProgressIndicator(new ProgressPieIndicator());
            image.showImage(curr_user.getCurr_ImageUrl());
            // if button is clicked, close the custom dialog
            ImageView dialogButton = dialog.findViewById(R.id.img_back);
            dialogButton.setOnClickListener(v -> dialog.dismiss());
            dialog.show();
        });

        title.setText("@" + curr_user.getCurr_Displayname());
        back.setOnClickListener(view1 -> nav.navigateUp());

        following_btn.setOnClickListener(view1 -> followClick("Following"));
        followers_btn.setOnClickListener(view1 -> followClick("Followers"));


        return view;
    }


    private void fileUploader() {
        ref = FirebaseStorage.getInstance().getReference().child("images/" + curr_user.getCurr_ID() + "/profile_image");
        ref.putFile(imgUri).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                ref.getDownloadUrl().addOnSuccessListener(uri -> {
                    downloadUri = uri;
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setPhotoUri(downloadUri).build();

                    user.updateProfile(profileUpdates).addOnCompleteListener(task1 -> {
                        if (task1.isSuccessful()) {
                            GlideApp.with(this).load(downloadUri).apply(new RequestOptions().circleCrop()).into(profile_image);
                            db.collection("Users").document(curr_user.getCurr_ID()).update("imgURL", downloadUri.toString());
                            Toast.makeText(getActivity(), "Profile Image updated", Toast.LENGTH_SHORT).show();
                        }
                    });
                });
            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                imgUri = result.getUri();
                fileUploader();
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }


    private void followClick(String follow) {
        Fragment fragment = new Fragment();
        Bundle bundle = new Bundle();
        bundle.putString("UserID", curr_user.getCurr_ID());
        bundle.putString("FollowKind", follow);
        fragment.setArguments(bundle);
        Navigation.findNavController(Objects.requireNonNull(getView()))
                .navigate(R.id.action_profileFragment_to_followFragment, bundle);
    }

    private void liveData() {
        //loading.setVisibility(View.VISIBLE);
        viewModel.getdataSnapshotLiveData().observe(this, arrayList -> {
            list.clear();
            list.addAll(arrayList);
            adapter.notifyDataSetChanged();
          //  loading.setVisibility(View.GONE);
        });

    }

}
