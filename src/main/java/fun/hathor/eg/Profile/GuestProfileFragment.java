package fun.hathor.eg.Profile;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.bumptech.glide.request.RequestOptions;
import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.indicator.progresspie.ProgressPieIndicator;
import com.github.piasy.biv.loader.glide.GlideImageLoader;
import com.github.piasy.biv.view.BigImageView;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;
import fun.hathor.eg.R;
import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.GlideApp;
import fun.hathor.eg.Classes.ImageZoom;
import fun.hathor.eg.Classes.Adapters.Posts_adapter;
import fun.hathor.eg.Classes.PostClass;
import fun.hathor.eg.Classes.ProfileViewModel.ProfileViewModel;
import fun.hathor.eg.Classes.ProfileViewModel.ProfileViewModelFactory;

public class GuestProfileFragment extends Fragment {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private TextView name_txt, following_btn, followers_btn;
    private ImageView profile_image, back;
    private RecyclerView recyclerView;
    private Posts_adapter adapter;
    private ArrayList<PostClass> list = new ArrayList<>();
    private String sentID , name , image;
    private LinearLayoutManager mLayoutManager;
    private LinearLayout follow_btn;
    private Curr_User curr_user;
    private TextView follow_txt;
    private NavController nav;
    private ImageView follow_ic;
    private ProfileViewModel viewModel;
    private NestedScrollView nestedScrollView;
    private ConstraintLayout parent_profile_img;


    public GuestProfileFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        BigImageViewer.initialize(GlideImageLoader.with(getContext()));
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        name_txt  = view.findViewById(R.id.toolbar_title);
        profile_image = view.findViewById(R.id.profile_image);
        recyclerView = view.findViewById(R.id.recycle_profile);
        mLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(mLayoutManager);
        adapter = new Posts_adapter(getActivity(), list, Posts_adapter.Kind.GUEST);
        recyclerView.setAdapter(adapter);
        follow_btn = view.findViewById(R.id.follow_btn);
        curr_user = new Curr_User();
        back = view.findViewById(R.id.back_arrow);
        follow_txt = view.findViewById(R.id.follow_txt);
        follow_ic = view.findViewById(R.id.follow_ic);
        followers_btn = view.findViewById(R.id.followers_btn);
        following_btn = view.findViewById(R.id.following_btn);
        parent_profile_img = view.findViewById(R.id.include3);
        nestedScrollView = view.findViewById(R.id.nested_scrollview);
        nav = Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment);
        /////////////////////////////
        //No logic Above here!
        ////////////////////////////

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            sentID = bundle.getString("sentID", null);}
        viewModel = ViewModelProviders.of(getActivity(), new ProfileViewModelFactory
                (getActivity().getApplication(), sentID)).get(ProfileViewModel.class);

        getInfo();
        checkFollow();
        follow_btn_fun();

        profile_image.setOnClickListener(view1 -> {
            // custom dialog
            final Dialog dialog = new Dialog(getContext(), R.style.Custom_Dialog);
            dialog.setContentView(R.layout.image_zoom);
            // set the custom dialog components - text, image and button
            BigImageView images = dialog.findViewById(R.id.imageView_large);
            images.setProgressIndicator(new ProgressPieIndicator());
            images.showImage(Uri.parse(image));
            // if button is clicked, close the custom dialog
            ImageView dialogButton = dialog.findViewById(R.id.img_back);
            dialogButton.setOnClickListener(v -> dialog.dismiss());
            dialog.show();
        });
        back.setOnClickListener(view1 -> nav.navigateUp());


        profile_image.setOnClickListener(view1 -> new ImageZoom(getContext() , image));

        following_btn.setOnClickListener(view1 -> followClick("Following"));
        followers_btn.setOnClickListener(view1 -> followClick("Followers"));

        liveData();

        nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener)
                (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                    if(scrollY == ( v.getChildAt(0).getMeasuredHeight() - v.getMeasuredHeight() )) {
                        viewModel.adddata();
                    }
                });

        return view;
    }

    private void getInfo(){
        db.collection("Users").document(sentID).get().addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                DocumentSnapshot document = task.getResult();
                name = document.getString("Username");
                image = document.getString("imgURL");
                    name_txt.setText("@"+name);
                    GlideApp.with(this).load(image)
                            .apply(new RequestOptions().circleCrop()).into(profile_image); }
        });
    }



    private void liveData() {
        viewModel.getdataSnapshotLiveData().observe(this, arrayList -> {
            list.clear();
            list.addAll(arrayList);
            adapter.notifyDataSetChanged();
        });

    }

       ////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////// ---------FOLLOW FUNCTIONS-------------- //////////////////////////////////

    private void follow_btn_fun(){

        follow_btn.setVisibility(View.VISIBLE);
        follow_btn.setOnClickListener(view1 ->{
            HashMap<String , String> followID = new HashMap();
            followID.put("FollowID" , sentID);
            followID.put("Username", name);
            followID.put("imgURL" , image);
            db.collection("Users").document(curr_user.getCurr_ID()).collection("Following").document(sentID)
                    .set(followID).addOnCompleteListener(task -> {
                if(task.isSuccessful()){
                    Toast.makeText(getContext(),"Following" ,Toast.LENGTH_LONG).show();
                    checkFollow();
                    HashMap<String , String> followerID = new HashMap();
                    followerID.put("FollowerID" , curr_user.getCurr_ID());
                    followerID.put("Username", curr_user.getCurr_Displayname());
                    followerID.put("imgURL" , curr_user.getCurr_ImageUrl().toString());
                    db.collection("Users").document(sentID).collection("Followers")
                            .document(curr_user.getCurr_ID()).set(followerID);
//                            .addOnSuccessListener(aVoid -> {
//                              db.collection("Users").document(new Curr_User().getCurr_ID()).collection() });
                }            });        });

    }

    private void followed(){
        follow_txt.setText("Following");
        follow_ic.setVisibility(View.GONE); }

    private void checkFollow(){
        db.collection("Users").document(curr_user.getCurr_ID()).collection("Following")
                .whereEqualTo("FollowID", sentID).get().addOnCompleteListener(task5 -> {
            if(task5.isSuccessful()){
                for(QueryDocumentSnapshot document : Objects.requireNonNull(task5.getResult())) {
                    if(Objects.equals(document.getString("FollowID"), sentID)){
                        followed();
                        Toast.makeText(getContext(),"Following" ,Toast.LENGTH_LONG).show();
                    }  }   }  });

    }

    private void followClick(String follow){
        Fragment fragment = new Fragment();
        Bundle bundle = new Bundle();
        bundle.putString("UserID", sentID);
        bundle.putString("FollowKind" , follow);
        fragment.setArguments(bundle);
        Navigation.findNavController(Objects.requireNonNull(getView()))
                .navigate(R.id.action_guestProfileFragment_to_followFragment, bundle);
    }
}
