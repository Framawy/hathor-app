package fun.hathor.eg.Profile;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.Objects;

import fun.hathor.eg.R;
import fun.hathor.eg.Classes.Adapters.User_adapter;
import fun.hathor.eg.Classes.UsersClass;

/**
 * A simple {@link Fragment} subclass.
 */
public class FollowFragment extends Fragment {

    private RecyclerView recyclerView;
    private User_adapter adapter;
    private String follow, userID;
    UsersClass user;
    private NavController nav;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private ArrayList<UsersClass> list = new ArrayList<>();
    private ArrayList<String> idlist = new ArrayList<>();

    public FollowFragment() {    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_follow, container, false);
        nav = Navigation.findNavController(getActivity() , R.id.nav_host_fragment);
        ImageView back = view.findViewById(R.id.back_arrow);
        back.setOnClickListener(view1 -> nav.navigateUp());
        TextView title = view.findViewById(R.id.toolbar_title);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            userID = bundle.getString("UserID", null);
            follow = bundle.getString("FollowKind", null);}

        title.setText(follow);
        recyclerView = view.findViewById(R.id.recycle_follow);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new User_adapter(getActivity(), list);
        recyclerView.setAdapter(adapter);

        getData();
        return view;
    }

    private void getData(){
        db.collection("Users").document(userID)
                .collection(follow).get().addOnCompleteListener(task -> {
                    if (task.isSuccessful()){
                        for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                            user = new UsersClass();
                            user.setUser_ID(document.getId());
                            user.setUsername(document.getString("Username"));
                            user.setImgURL(document.getString("imgURL"));
                            list.add(user); }
                        adapter.notifyDataSetChanged(); }
                });
    }

}
