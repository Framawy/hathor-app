package fun.hathor.eg.Classes.HomeViewModel;


import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;
import java.util.Collections;

import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.PostClass;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class HomeViewModel extends ViewModel {
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private int limit = 10;
    private Curr_User curr_user;
    private PostClass postClass;
    private ArrayList<String> loveIDs = new ArrayList<>();
    private MutableLiveData<ArrayList<PostClass>> postsList;
    private ArrayList<String> postIDs = new ArrayList<>();
    private ArrayList<PostClass> list = new ArrayList<>();
    private ArrayList<PostClass> temp = new ArrayList<>();
    private DocumentSnapshot lastVisible;
    private boolean noMORE = false;

    public HomeViewModel() {
        curr_user = new Curr_User();
    }

    @NonNull
    public LiveData<ArrayList<PostClass>> getdataSnapshotLiveData() {
        if (postsList == null) {
            postsList = new MutableLiveData<>();
            loaddata();
        }
        return postsList;
    }


    public void loaddata() {
        db.collection("Users")
                .document(curr_user.getCurr_ID()).collection("Timeline")
                .orderBy("time", Query.Direction.DESCENDING).limit(limit).get().addOnSuccessListener(documentSnapshots -> {
            for (DocumentSnapshot document : documentSnapshots.getDocuments()) {
                postIDs.add(document.getId());
            }
            if (!postIDs.isEmpty()) {
                lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);
            }
            for (String s : postIDs) {
                db.collection("Posts").document(s).get().addOnSuccessListener(documentSnapshot -> {
                    postClass = documentSnapshot.toObject(PostClass.class);
                    loveIDs = (ArrayList<String>) documentSnapshot.get("loves");
                    if (loveIDs != null) {
                        if (loveIDs.contains(curr_user.getCurr_ID())) {
                            postClass.setLove(true);
                        }
                    }
                    list.add(postClass);
                    Collections.sort(list);
                    postsList.postValue(list);
                });

            }
            if(documentSnapshots.size() < limit){ noMORE = true; }

        }).addOnFailureListener(
                e -> Log.w(TAG, "createUserWithEmail:failure", e.getCause()));

    }


    public void adddata() {
        if(!noMORE){
        db.collection("Users")
                .document(curr_user.getCurr_ID()).collection("Timeline")
                .orderBy("time", Query.Direction.DESCENDING).startAfter(lastVisible).limit(limit).get()
                .addOnSuccessListener(documentSnapshots -> {
            for (DocumentSnapshot document : documentSnapshots.getDocuments()) {
                postIDs.clear();
                postIDs.add(document.getId());
            }
            if (documentSnapshots.size() != 0) {
                lastVisible = documentSnapshots.getDocuments().get(documentSnapshots.size() - 1);

            }if(documentSnapshots.size() < limit){ noMORE = true; }
            for (String s : postIDs) {
                db.collection("Posts").document(s).get().addOnSuccessListener(documentSnapshot -> {
                    postClass = documentSnapshot.toObject(PostClass.class);
                    loveIDs = (ArrayList<String>) documentSnapshot.get("Loves");
                    if (loveIDs != null) {
                        if (loveIDs.contains(curr_user.getCurr_ID())) {
                            postClass.setLove(true);
                        }
                    }
                    temp.add(postClass);
                    Collections.sort(temp);
                    list.addAll(temp);
                    postsList.postValue(list);
                });

            }


        }).addOnFailureListener(
                e -> Log.w(TAG, "createUserWithEmail:failure", e.getCause()));
    }}

    public void refresh(){
        loaddata();
    }
}
