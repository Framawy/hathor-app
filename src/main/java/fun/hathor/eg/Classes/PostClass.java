package fun.hathor.eg.Classes;


import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.ServerTimestamp;

import java.util.Date;

public class PostClass implements Comparable<PostClass> {
    private String userID, userName, userIMG;
    private String postID , postIMG, postTXT;
    private int lovesCount = 0;
    private boolean love = false;
    private String[] loves;
    @ServerTimestamp private Date time;


    public PostClass() { }

    public PostClass(String userID, String userName, String userIMG, String postID, String postIMG, String postTXT) {
        this.userID = userID;
        this.userName = userName;
        this.userIMG = userIMG;
        this.postID = postID;
        this.postIMG = postIMG;
        this.postTXT = postTXT;
    }

    public PostClass(String userID, String userName, String userIMG, String postID, String postIMG, String postTXT,  boolean love) {
        this.userID = userID;
        this.userName = userName;
        this.userIMG = userIMG;
        this.postID = postID;
        this.postIMG = postIMG;
        this.postTXT = postTXT;
        this.love = love;
    }


    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserIMG() {
        return userIMG;
    }

    public void setUserIMG(String userIMG) {
        this.userIMG = userIMG;
    }

    public String getPostID() {
        return postID;
    }

    public void setPostID(String postID) {
        this.postID = postID;
    }

    public String getPostIMG() {
        return postIMG;
    }

    public void setPostIMG(String postIMG) {
        this.postIMG = postIMG;
    }

    public String getPostTXT() {
        return postTXT;
    }

    public void setPostTXT(String postTXT) {
        this.postTXT = postTXT;
    }

    public int getLovesCount() {
        return lovesCount;
    }

    public void setLovesCount(int lovesCount) {
        this.lovesCount = lovesCount;
    }
    @Exclude
    public boolean isLove() {
        return love;
    }

    @Exclude
    public void setLove(boolean love) {
        this.love = love;
    }
    @Exclude
    public String[] getLoves() {
        return loves;
    }
    @Exclude
    public void setLoves(String[] loves) {
        this.loves = loves;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }



    @Override
    public int compareTo(PostClass compare) {
        if (getTime() == null || compare.getTime() == null)
            return 0;
        return compare.getTime().compareTo(getTime());
    }
}
