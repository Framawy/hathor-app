package fun.hathor.eg.Classes.Adapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexboxLayoutManager;

import java.util.ArrayList;
import fun.hathor.eg.R;
import fun.hathor.eg.Classes.GlideApp;
import fun.hathor.eg.Classes.SponsorClass;

public  class SponsorAdapter extends RecyclerView.Adapter<SponsorAdapter.ViewHolder> {

    private ArrayList<SponsorClass> list;
    private Context context;

    // data is passed into the constructor
    public SponsorAdapter(Context context, ArrayList<SponsorClass> list) {
        this.list = list;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public SponsorAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.sponsor_cell, parent, false);
        FlexboxLayoutManager.LayoutParams params = (FlexboxLayoutManager.LayoutParams) view.getLayoutParams();
        params.width  = (parent.getMeasuredWidth() / 3) - 8 * 3;
        //params.height = (parent.getMeasuredHeight() /2) - 8;
        view.setLayoutParams(params);
        return new SponsorAdapter.ViewHolder(view);
    }

    // binds the data to View in each row
    @Override
    public void onBindViewHolder(SponsorAdapter.ViewHolder holder, int position) {
        holder.name.setText(list.get(position).getSponsorName());
        GlideApp.with(context)
                .load(list.get(position).getSponsorImg())
                .into(holder.image); }

    @Override
    public int getItemCount() { return list.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        ImageView image;
        ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.sponsor_txtView);
            image = itemView.findViewById(R.id.sponsor_imgView);
        }
    }
}
