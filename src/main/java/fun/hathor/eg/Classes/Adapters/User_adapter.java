package fun.hathor.eg.Classes.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.R;
import fun.hathor.eg.Classes.GlideApp;
import fun.hathor.eg.Classes.UsersClass;

public class User_adapter extends RecyclerView.Adapter<User_adapter.ViewHolder> implements Filterable {


    private User_adapter.ItemClickListener mClickListener;
    private Context context;
    private int flag = 0;
    private Curr_User curr_user= new Curr_User();
    private ArrayList<UsersClass> userListFiltered = new ArrayList<>();
    private ArrayList<UsersClass> userlist = new ArrayList<>();
    private ArrayList<UsersClass> list;

    // data is passed into the constructor
    public User_adapter(Context context, ArrayList<UsersClass> list) {
        this.list = list;
        this.context = context;
    }

    //Notification constructor
    public User_adapter(Context context, ArrayList<UsersClass> list, int flag) {
        this.list = list;
        this.context = context;
        this.flag = flag;
    }

    //Search constructor
    public User_adapter(Context context, ArrayList<UsersClass> list, ArrayList<UsersClass> userlist) {
        this.list = list;
        this.context = context;
        this.userlist = userlist;
    }
    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public User_adapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new User_adapter.ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user_cell, parent, false));
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(User_adapter.ViewHolder holder, int position) {
        if (flag == 1) {
            holder.likeUrPic.setVisibility(View.VISIBLE);
        }
        if(!list.isEmpty()){
        holder.username.setText(list.get(position).getUsername());
        GlideApp.with(context).load(list.get(position).getImgURL()).placeholder(R.color.lightGrey).apply(new RequestOptions().circleCrop()).into(holder.user_img);}
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return  list == null ? 0 : list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ConstraintLayout parent;
        TextView username, likeUrPic;
        ImageView user_img;

        ViewHolder(View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.class_username);
            user_img = itemView.findViewById(R.id.class_userimg);
            likeUrPic = itemView.findViewById(R.id.liked_your_pic);
            parent = itemView.findViewById(R.id.user_cell_parent);

            parent.setOnClickListener(v -> {
                String s = list.get(getAdapterPosition()).getUser_ID();
                if(s.equals(curr_user.getCurr_ID())){
                    Navigation.findNavController(itemView).navigate(R.id.profileFragment); }
                else {
                Fragment fragment = new Fragment();
                Bundle bundle = new Bundle();
                bundle.putString("sentID", s);
                fragment.setArguments(bundle);
                Navigation.findNavController(itemView).navigate(R.id.action_searchFragment_to_guestProfileFragment, bundle);}
            });

        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    int getItem(int position) {
        return position;
    }

    // allows clicks events to be caught
    void setClickListener(User_adapter.ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    String timecheck(String x) {
        if (TextUtils.isEmpty(x)) {
            return "null";
        } else return x;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    userListFiltered = list;
                } else {
                    ArrayList<UsersClass> filteredList = new ArrayList<>();
                    for (UsersClass user : userlist) {

                        // name match condition. this might differ depending on your requirement
                        // here we are looking for name or phone number match
                        String s = user.getUsername();
                        if (user.getUsername().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(user);
                            int x = 10;
                        }
                    }

                    userListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = userListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                userListFiltered = (ArrayList<UsersClass>) filterResults.values;
                list = userListFiltered;
                // refresh the list with filtered data
                notifyDataSetChanged();
            }
        };
    }


}
