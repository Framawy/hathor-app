package fun.hathor.eg.Classes.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.request.RequestOptions;
import com.github.piasy.biv.indicator.progresspie.ProgressPieIndicator;
import com.github.piasy.biv.view.BigImageView;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import fun.hathor.eg.R;
import fun.hathor.eg.Service.LikeService;
import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.GlideApp;
import fun.hathor.eg.Classes.PostClass;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Posts_adapter extends RecyclerView.Adapter<Posts_adapter.ViewHolder> {

    private Curr_User curr_user;
    private ItemClickListener mClickListener;
    private ArrayList<PostClass> list;
    private Context context;

    private FirebaseFirestore db = FirebaseFirestore.getInstance();

    public enum Kind {HOME, PROFILE, GUEST}

    private Kind kind;

    // data is passed into the constructor
    public Posts_adapter(Context context, ArrayList<PostClass> list, Kind kind) {
        this.list = list;
        this.context = context;
        this.kind = kind;
    }

    // inflates the row layout from xml when needed
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.post_cell, parent, false));
    }

    // binds the data to View in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.username.setText(list.get(position).getUserName());
        holder.post_txt.setText(list.get(position).getPostTXT());
        if(list.get(position).getPostTXT().equals("")){
            holder.post_txt.setVisibility(View.GONE);
        }
        holder.time.setText(timecheck(list.get(position).getTime()));

        GlideApp.with(context).load(list.get(position).getUserIMG()).apply(new RequestOptions().circleCrop()).into(holder.user_img);
        GlideApp.with(context).load(list.get(position).getPostIMG()).into(holder.post_img);

        if (list.get(position).isLove()) {
            holder.love.setChecked(true);
        } else {
            holder.love.setChecked(false);
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView username, post_txt, time;
        ImageView user_img, post_img;
        ToggleButton love;
        ConstraintLayout topview;

        ViewHolder(View itemView) {
            super(itemView);
            username = itemView.findViewById(R.id.post_username);
            post_txt = itemView.findViewById(R.id.post_txt);
            user_img = itemView.findViewById(R.id.post_user_img);
            post_img = itemView.findViewById(R.id.post_img);
            time = itemView.findViewById(R.id.timestamp_txt);
            love = itemView.findViewById(R.id.love_btn);
            topview = itemView.findViewById(R.id.postUserView);

            curr_user = new Curr_User();

            topview.setOnClickListener(view -> {

                String userid = list.get(getAdapterPosition()).getUserID();
                Fragment fragment = new Fragment();
                Bundle bundle = new Bundle();
                bundle.putString("sentID", userid);
                fragment.setArguments(bundle);
                Navigation.findNavController(itemView).navigate(R.id.guestProfileFragment, bundle);

            });
            post_img.setOnClickListener(view -> {
                // custom dialog
                final Dialog dialog = new Dialog(context, R.style.Custom_Dialog);
                dialog.setContentView(R.layout.image_zoom);
                // set the custom dialog components - text, image and button
                BigImageView image = dialog.findViewById(R.id.imageView_large);
                image.setProgressIndicator(new ProgressPieIndicator());
                image.showImage(Uri.parse(list.get(getAdapterPosition()).getPostIMG()));
                // if button is clicked, close the custom dialog
                ImageView dialogButton = dialog.findViewById(R.id.img_back);
                dialogButton.setOnClickListener(v -> dialog.dismiss());
                dialog.show();
            });

            love.setOnClickListener(view -> {
                WriteBatch batch = db.batch();
                if (!love.isChecked()) {//unlike
                    db.collection("Posts").document(list.get(getAdapterPosition()).getPostID())
                            .update("loves", FieldValue.arrayRemove(curr_user.getCurr_ID()))
                            .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {
                                    love.setChecked(false);
                                    list.get(getAdapterPosition()).setLove(false);
                                    Intent intent = new Intent(context, LikeService.class);
                                    intent.putExtra("UserID", list.get(getAdapterPosition()).getUserID());
                                    intent.putExtra("PostID", list.get(getAdapterPosition()).getPostID());
                                    intent.putExtra("like_unlike", 0);
                                    context.startService(intent);
                                } else {
                                    Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                }
                            });


                } else {
                    //like
                    db.collection("Posts").document(list.get(getAdapterPosition()).getPostID()).update("loves", FieldValue.arrayUnion(curr_user.getCurr_ID()))
                            .addOnCompleteListener(task -> {
                                if (task.isSuccessful()) {
                                    love.setChecked(true);
                                    list.get(getAdapterPosition()).setLove(true);
                                    Intent intent = new Intent(context, LikeService.class);
                                    intent.putExtra("UserID", list.get(getAdapterPosition()).getUserID());
                                    intent.putExtra("PostID", list.get(getAdapterPosition()).getPostID());
                                    intent.putExtra("like_unlike", 1);
                                    context.startService(intent);
                                }
                            });


                }
            });
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    int getItem(int position) {
        return position;
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    private String timecheck(Date x) {
        SimpleDateFormat simpleDateFormat;
        simpleDateFormat = new SimpleDateFormat("h a, E dd/MMM", Locale.US);
        return simpleDateFormat.format(x);
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

}


