package fun.hathor.eg.Classes;



public class UsersClass {

    private String user_ID, username, imgURL;

    public UsersClass() { }

    public UsersClass(String user_ID, String username, String imgURL) {
        this.user_ID = user_ID;
        this.username = username;
        this.imgURL = imgURL;
    }

    public String getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(String user_ID) {
        this.user_ID = user_ID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImgURL() {
        return imgURL;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }
}
