package fun.hathor.eg.Classes;


public class SponsorClass {
    String sponsorName , sponsorImg;

    public SponsorClass() { }

    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

    public String getSponsorImg() {
        return sponsorImg;
    }

    public void setSponsorImg(String sponsorImg) {
        this.sponsorImg = sponsorImg;
    }
}
