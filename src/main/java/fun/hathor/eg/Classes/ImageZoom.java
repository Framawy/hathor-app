package fun.hathor.eg.Classes;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.widget.ImageView;

import com.github.piasy.biv.indicator.progresspie.ProgressPieIndicator;
import com.github.piasy.biv.view.BigImageView;

import fun.hathor.eg.R;

public class ImageZoom {

    private Context context;
    private String link;

    public ImageZoom(Context context, String link) {
        this.context = context;
        this.link = link;

        final Dialog dialog = new Dialog(context, R.style.Custom_Dialog);
        dialog.setContentView(R.layout.image_zoom);
        // set the custom dialog components - text, image and button
        BigImageView image = dialog.findViewById(R.id.imageView_large);
        image.setProgressIndicator(new ProgressPieIndicator());
        image.showImage(Uri.parse(link));
        // if button is clicked, close the custom dialog
        ImageView dialogButton = dialog.findViewById(R.id.img_back);
        dialogButton.setOnClickListener(v -> dialog.dismiss());
        dialog.show(); }

    }



