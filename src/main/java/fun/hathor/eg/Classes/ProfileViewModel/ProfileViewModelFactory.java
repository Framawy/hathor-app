package fun.hathor.eg.Classes.ProfileViewModel;

import android.app.Application;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class ProfileViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private String userID;


    public ProfileViewModelFactory(Application application, String userID) {
        mApplication = application;
        this.userID = userID;
    }


    @Override
    public <T extends ViewModel> T create(Class<T> modelClass) {
        return (T) new ProfileViewModel(userID);
    }
}
