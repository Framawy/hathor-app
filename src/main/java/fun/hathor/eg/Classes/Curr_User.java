package fun.hathor.eg.Classes;

import android.net.Uri;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class Curr_User {
        private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


    public Curr_User() { }

    public String getCurr_Displayname() {
        return user.getDisplayName();
    }

    public String getCurr_Email() {
        return user.getEmail();
    }

    public String getCurr_ID() {
        return user.getUid();
    }

    public Uri getCurr_ImageUrl(){
        return user.getPhotoUrl();}

}
