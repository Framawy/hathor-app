package fun.hathor.eg.Sign_fragments;


import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import java.util.Objects;
import fun.hathor.eg.MainActivity;
import fun.hathor.eg.R;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class SignINFragment extends Fragment {

    private FirebaseAuth mAuth;
    private  String email , password;
    private EditText email_edittxt , password_edittxt;
    private NavController sign_nav;
    public SignINFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        mAuth = FirebaseAuth.getInstance();

        email_edittxt = view.findViewById(R.id.email_edittext);
        password_edittxt = view.findViewById(R.id.password_edittext);
        TextView to_signUp_btn = view.findViewById(R.id.to_signUp_btn);
        Button signIn_btn = view.findViewById(R.id.signin_btn);

        signIn_btn.setOnClickListener(view1 -> {
            email = email_edittxt.getText().toString();
            password = password_edittxt.getText().toString();
            check(email , password);
        });

        to_signUp_btn.setOnClickListener(view12 -> {
            sign_nav = Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.sign_nav_host_fragment);
            sign_nav.navigate(R.id.action_signINFragment_to_signUPFragment);
        });

        return view;
    }

    private void check(String email , String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        startActivity(new Intent(getActivity() , MainActivity.class));
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(getActivity(), "Authentication failed.",
                                Toast.LENGTH_SHORT).show();

                    }

                });
    }

}
