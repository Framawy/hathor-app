package fun.hathor.eg.Sign_fragments;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.firestore.FirebaseFirestore;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import fun.hathor.eg.MainActivity;
import fun.hathor.eg.R;
import static androidx.constraintlayout.widget.Constraints.TAG;

public class SignUPFragment extends Fragment {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private String email ,username, password;
    private EditText email_edittxt , username_edittxt ,  password_edittxt;


    public SignUPFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        mAuth = FirebaseAuth.getInstance();

        email_edittxt = view.findViewById(R.id.email_edittext);
        username_edittxt = view.findViewById(R.id.username_edittext);
        password_edittxt = view.findViewById(R.id.password_edittext);
        Button signUP_btn = view.findViewById(R.id.signUP_btn);

        signUP_btn.setOnClickListener(view1 -> {
            email = email_edittxt.getText().toString();
            password = password_edittxt.getText().toString();
            username = username_edittxt.getText().toString();
            check(email , username ,password);
        });
        return view;
    }



    @SuppressLint("RestrictedApi")
    private void check(String email , String username , String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        FirebaseUser user = mAuth.getCurrentUser();
                        assert user != null;
                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                                .setDisplayName(username.toLowerCase())
                                .build();

                        user.updateProfile(profileUpdates)
                                .addOnCompleteListener(task1 -> {
                                    if (task1.isSuccessful()) {
                                        Map<String, String> name_map = new HashMap<>();
                                        name_map.put("Username", username);
                                        db.collection("Users").document(user.getUid()).set(name_map);
                                        startActivity(new Intent(getActivity() , MainActivity.class));
                                    }
                                });

                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "createUserWithEmail:failure", task.getException());
                        Toast.makeText(getActivity() , "Authentication failed.",
                                Toast.LENGTH_SHORT).show();

                    }


                });
    }

}
