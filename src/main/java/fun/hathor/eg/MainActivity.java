package fun.hathor.eg;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.gauravk.bubblenavigation.BubbleNavigationLinearView;


import java.util.ArrayList;
import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.PostClass;

public class MainActivity extends AppCompatActivity {


    Curr_User curr_user;
    NavController nav;
    ImageView back;
    TextView title;
    ConstraintLayout toolbar_parent;
    ArrayList<PostClass> list = new ArrayList<>();

    @Override
    public void onBackPressed() {
        nav.navigateUp();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        curr_user = new Curr_User();
        data();
        nav = Navigation.findNavController(this , R.id.nav_host_fragment);
        BubbleNavigationLinearView botbar   = findViewById(R.id.bottom_bar);

        back = findViewById(R.id.back_arrow);
        toolbar_parent = findViewById(R.id.include);
        title = findViewById(R.id.toolbar_title);
        back.setOnClickListener(view -> nav.navigateUp());


        botbar.setNavigationChangeListener((view, position) -> {
            switch (position){
                case 0 :   nav.navigate(R.id.homeFragment);         break;
                case 1 :   nav.navigate(R.id.trendsFragment);       break;
                case 2 :   nav.navigate(R.id.searchFragment);       break;
                case 3 :   nav.navigate(R.id.notificationFragment); break;
                case 4 :   nav.navigate(R.id.settingsFragment);     break;
            }
        });

        nav.addOnDestinationChangedListener((controller, destination, arguments) -> {

            switch (destination.getId()){
                case (R.id.homeFragment) :
                    title.setText("Hathor");
                    back.setVisibility(View.INVISIBLE);
                    toolbar_parent.setVisibility(View.VISIBLE);
                    botbar.setCurrentActiveItem(0);
                    botbar.setVisibility(View.VISIBLE);break;

                case (R.id.trendsFragment) :
                    back.setVisibility(View.INVISIBLE);
                    title.setText("Trends");
                    botbar.setCurrentActiveItem(1);
                    toolbar_parent.setVisibility(View.VISIBLE);
                    botbar.setVisibility(View.VISIBLE);break;

                case (R.id.searchFragment) :
                    back.setVisibility(View.INVISIBLE);
                    title.setText("Search");
                    botbar.setCurrentActiveItem(2);
                    toolbar_parent.setVisibility(View.VISIBLE);
                    botbar.setVisibility(View.VISIBLE);break;

                case (R.id.notificationFragment) :
                    back.setVisibility(View.INVISIBLE);
                    title.setText("Notify");
                    botbar.setCurrentActiveItem(3);
                    toolbar_parent.setVisibility(View.VISIBLE);
                    botbar.setVisibility(View.VISIBLE);break;

                case (R.id.settingsFragment) :
                    back.setVisibility(View.INVISIBLE);
                    title.setText("Settings");
                    botbar.setCurrentActiveItem(4);
                    toolbar_parent.setVisibility(View.VISIBLE);
                    botbar.setVisibility(View.VISIBLE);break;

                    ////////////////Side Fragments////////////////////

                case (R.id.newPostFragment) :
                    back.setVisibility(View.VISIBLE);
                    title.setText("Hathor");
                    toolbar_parent.setVisibility(View.VISIBLE);
                    botbar.setVisibility(View.GONE);break;


                case (R.id.addSponsorFragment) :
                    back.setVisibility(View.VISIBLE);
                    title.setText("SponsorShip");
                    botbar.setVisibility(View.GONE);break;

                case (R.id.profileFragment) :
                case (R.id.guestProfileFragment) :
                    toolbar_parent.setVisibility(View.GONE);
                    botbar.setVisibility(View.GONE);break;

            }
        });



    }

    private void data(){
        //HomeViewModel homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
       // homeViewModel.getdataSnapshotLiveData().observe(this, postList -> list.addAll(postList));


    }
}

