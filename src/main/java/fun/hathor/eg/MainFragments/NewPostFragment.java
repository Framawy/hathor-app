package fun.hathor.eg.MainFragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Objects;
import fun.hathor.eg.R;
import fun.hathor.eg.Classes.GlideApp;

import fun.hathor.eg.Service.UploadPostService;

public class NewPostFragment extends Fragment {

    public NewPostFragment() {  }

    private Button share;
    private ImageView image;
    private ConstraintLayout parentimg;
    private TextView addtext;
    private EditText txt;
    private Uri imgUri;
    private NavController nav;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_post, container, false);

        share = view.findViewById(R.id.share_btn);
        image = view.findViewById(R.id.newPostImage);
        txt = view.findViewById(R.id.newPost_editText);

        nav = Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment);

       image.setOnClickListener(view1 -> file_grab());
        file_grab();
        share.setOnClickListener(view1 -> {
            if(imgUri == null){
                Toast.makeText(getContext(), "Please choose Image", Toast.LENGTH_SHORT).show(); }
            else{
            Intent intent = new Intent(getContext(), UploadPostService.class);
            intent.putExtra("IMG_URI" , imgUri.toString());
            intent.putExtra("POST_TXT" , txt.getText().toString().replaceAll( "<br />" , "\\n"));
            getActivity().startService(intent);
            nav.navigateUp(); }});

        return view;
    }


    private void file_grab(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent , 1); }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {

        final int unmaskedRequestCode = requestCode & 0x0000ffff;
        if(requestCode == 1 && unmaskedRequestCode == 1 && data != null && data.getData() != null){
            imgUri = data.getData();
            GlideApp.with(this).load(imgUri).into(image);
        }
    }


}
