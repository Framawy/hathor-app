package fun.hathor.eg.MainFragments;


import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.github.piasy.biv.BigImageViewer;
import com.github.piasy.biv.loader.glide.GlideImageLoader;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import java.util.ArrayList;
import java.util.Objects;
import fun.hathor.eg.R;
import fun.hathor.eg.Classes.Adapters.Posts_adapter;
import fun.hathor.eg.Classes.HomeViewModel.HomeViewModel;
import fun.hathor.eg.Classes.PostClass;

import static android.content.Context.MODE_PRIVATE;

public class HomeFragment extends Fragment {


    public HomeFragment() { }
    private NavController nav;
    private RecyclerView recycle;
    private Posts_adapter adapter;
    private FloatingActionButton fab;
    private ArrayList<PostClass> list = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private HomeViewModel viewModel;
    private boolean isScrolling = false;
    private boolean isLastItemReached = false;
    private int firstVisiblePosition, offset;
    private String jsonlist, key = "key";
    private ShimmerFrameLayout loading;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        BigImageViewer.initialize(GlideImageLoader.with(getContext()));
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        fab = view.findViewById(R.id.floatingActionButton);
        nav = Navigation.findNavController(Objects.requireNonNull(getActivity()), R.id.nav_host_fragment);
        loading = view.findViewById(R.id.post_loading_view);
        loading.startShimmer();
        sharedPreferences = getActivity().getSharedPreferences("HathorApp", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        SwipeRefreshLayout refresh = view.findViewById(R.id.homerefresh);

        refresh.setOnRefreshListener(() -> {
            viewModel.refresh();
            refresh.setRefreshing(false);
        });
        fab.setOnClickListener(view1 -> nav.navigate(R.id.action_homeFragment_to_newPostFragment));


        viewModel = ViewModelProviders.of(getActivity()).get(HomeViewModel.class);

        recycle = view.findViewById(R.id.recycle_posts);
        mLayoutManager = new LinearLayoutManager(getContext());
        recycle.setLayoutManager(mLayoutManager);
        adapter = new Posts_adapter(getContext(), list, Posts_adapter.Kind.HOME);
        recycle.setAdapter(adapter);


        recycle.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                    isScrolling = true;
                }
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = ((LinearLayoutManager) recyclerView.getLayoutManager());
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();

                if (isScrolling && (firstVisibleItemPosition + visibleItemCount == totalItemCount) && !isLastItemReached) {
                    isScrolling = false;
                    viewModel.adddata();
                }
            }
        });


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getposts();
    }


    @Override
    public void onResume() {
        recycle.scrollToPosition(sharedPreferences.getInt("position", 0));
        new Handler().postDelayed(() -> recycle.scrollBy(0, -sharedPreferences.getInt("offset", 0)), 10);
        super.onResume();
    }

    @Override
    public void onPause() {
        if (!list.isEmpty()) {
            View firstChild = recycle.getChildAt(0);
            firstVisiblePosition = recycle.getChildAdapterPosition(firstChild);
            offset = firstChild.getTop();
            editor.putInt("position", firstVisiblePosition)
                    .putInt("offset", offset)
                    .putString(key, jsonlist)
                    .apply(); }
        super.onPause();
    }


    private void getposts(){
        viewModel.getdataSnapshotLiveData().observe(Objects.requireNonNull(getActivity()), posts_list -> {
            list.clear();
            list.addAll(posts_list);
            adapter.notifyDataSetChanged();
            loading.stopShimmer();
            loading.setVisibility(View.GONE);
        });


    }
}
