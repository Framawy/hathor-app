package fun.hathor.eg.MainFragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.flexbox.AlignItems;
import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexWrap;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.ArrayList;

import fun.hathor.eg.R;
import fun.hathor.eg.Classes.Adapters.Posts_adapter;
import fun.hathor.eg.Classes.Adapters.SponsorAdapter;
import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.PostClass;
import fun.hathor.eg.Classes.SponsorClass;

public class DiscoverFragment extends Fragment {
    private SponsorAdapter sponsorAdapter;
    private Posts_adapter adapterTrends;
    private RecyclerView recyclerSponsor , recycleTrends;
    private FirebaseFirestore db =  FirebaseFirestore.getInstance();
    private ArrayList<SponsorClass> sponsorlist = new ArrayList<>();
    private ArrayList<PostClass> trendinglist = new ArrayList<>();
    private ArrayList<String> loveIDs = new ArrayList<>();
    private SponsorClass sponsorClass = new SponsorClass();
    private PostClass posts_class = new PostClass();
    private Curr_User curr_user;
    public DiscoverFragment() { }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_trend, container, false);
        curr_user = new Curr_User();
        recycleTrends = view.findViewById(R.id.recycler_trending);
        recycleTrends.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterTrends = new Posts_adapter(getContext(), trendinglist , Posts_adapter.Kind.HOME);
        recycleTrends.setAdapter(adapterTrends);


        recyclerSponsor = view.findViewById(R.id.recycle_sponser);
        FlexboxLayoutManager flex = new FlexboxLayoutManager(getContext());
        flex.setFlexDirection(FlexDirection.COLUMN);
        flex.setFlexWrap(FlexWrap.WRAP);
        flex.setJustifyContent(JustifyContent.FLEX_START);
        flex.setAlignItems(AlignItems.CENTER);

        recyclerSponsor.setLayoutManager(flex);
        sponsorAdapter = new SponsorAdapter(getContext(), sponsorlist);
        recyclerSponsor.setAdapter(sponsorAdapter);
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerSponsor);





        getSponsors();
        getTrending();

        return view;
    }


    private void getSponsors(){
        db.collection("Sponsors").get().addOnSuccessListener(documentSnapshots -> {
            for (DocumentSnapshot document : documentSnapshots.getDocuments()) {
                sponsorClass = document.toObject(SponsorClass.class);
                sponsorlist.add(sponsorClass);
            }
            sponsorAdapter.notifyDataSetChanged();
        });
    }

    private void getTrending(){
        db.collection("Posts").orderBy("lovesCount" , Query.Direction.DESCENDING)
                .get().addOnSuccessListener(documentSnapshots -> {
            for (DocumentSnapshot document : documentSnapshots.getDocuments()) {
                posts_class = document.toObject(PostClass.class);
                loveIDs = (ArrayList<String>) document.get("loves");
                if (loveIDs != null) {
                    if (loveIDs.contains(curr_user.getCurr_ID())) {
                        posts_class.setLove(true); }
                } else {
                    posts_class.setLove(false); }
                trendinglist.add(posts_class);
            }
            adapterTrends.notifyDataSetChanged();
        });
    }


}
