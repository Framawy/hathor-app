package fun.hathor.eg.MainFragments;


import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.Objects;

import fun.hathor.eg.R;
import fun.hathor.eg.Classes.Adapters.User_adapter;
import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.UsersClass;

public class NotificationFragment extends Fragment {


    public NotificationFragment() {}

    private ArrayList<UsersClass> list = new ArrayList<>();
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Curr_User curr_user = new Curr_User();
    private UsersClass user = new UsersClass();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        RecyclerView recyclerView = view.findViewById(R.id.notify_recycle);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        // int 1 in adapter makes the String Liked your pic visible
        User_adapter adapter =  new User_adapter(getContext() , list , 1);
        recyclerView.setAdapter(adapter);

       db.collection("Users").document(curr_user.getCurr_ID()).collection("Notifications")
               .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()){
                for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                    user.setUser_ID(document.getString("user_ID"));
                    user.setUsername(document.getString("username"));
                    user.setImgURL(document.getString("imgURL"));
                    list.add(user); }

                adapter.notifyDataSetChanged();}
        });



        return view;
    }




}
