package fun.hathor.eg.MainFragments;

import android.os.Bundle;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import java.util.ArrayList;
import java.util.Objects;
import fun.hathor.eg.R;
import fun.hathor.eg.Classes.Adapters.User_adapter;
import fun.hathor.eg.Classes.UsersClass;
import static androidx.constraintlayout.widget.Constraints.TAG;

public class SearchFragment extends Fragment {

    public SearchFragment() { }

    private SearchView search_bar;
    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private RecyclerView recyclerView;
    private ArrayList<UsersClass> list = new ArrayList<>();
    private ArrayList<UsersClass> usersList = new ArrayList<>();
    private User_adapter adapter;
    private UsersClass user;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        search_bar = view.findViewById(R.id.search_edittxt);
        recyclerView = view.findViewById(R.id.search_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new User_adapter(getActivity(), list, usersList);
        recyclerView.setAdapter(adapter);
        user = new UsersClass();
        getUsers();
        search_bar.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 2) {
                    for (UsersClass user : usersList) {
                        if (user.getUsername().toLowerCase().contains(newText.toLowerCase())) {
                            list.add(user);
                        }
                        adapter.notifyDataSetChanged();
                    }
                }
                return false;
            }

        });
        return view;

    }

    private void getUsers() {
        db.collection("Users").get()
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                            Log.d(TAG, document.getId() + " => " + document.getData());
                            user = new UsersClass();
                            user.setUser_ID(document.getId());
                            user.setUsername(document.getString("Username"));
                            user.setImgURL(document.getString("imgURL"));
                            usersList.add(user);
                        }
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.getException());
                    }
                });
    }

    @Override
    public void onResume() {
        list.clear();
        adapter.notifyDataSetChanged();
        super.onResume();
    }
}
