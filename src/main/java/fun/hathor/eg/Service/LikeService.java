package fun.hathor.eg.Service;



import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.WriteBatch;
import java.util.HashMap;
import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.UsersClass;

import static androidx.constraintlayout.widget.Constraints.TAG;


public class LikeService extends Service {

    private FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Curr_User curr_user = new Curr_User();

    private String lovesCount;
    private int flag;

    public LikeService() { }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // notification();
        int num = intent.getIntExtra("like_unlike", 1);
        String userId, postID;
        userId = intent.getStringExtra("UserID");
        postID = intent.getStringExtra("PostID");

        switch (num) {
            case 1:
                like(userId, postID);
                break;
            case 0:
                unlike(userId, postID);
                break;
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "Done Like/Unlike", Toast.LENGTH_LONG).show();
    }

    private void like(String userID, String postID) {
        UsersClass usersClass = new UsersClass();
        usersClass.setUser_ID(curr_user.getCurr_ID());
        usersClass.setUsername(curr_user.getCurr_Displayname());
        usersClass.setImgURL(curr_user.getCurr_ImageUrl().toString());
        db.collection("Users").document(userID)
                .collection("Notifications").document(postID).set(usersClass).addOnSuccessListener(aVoid ->
                db.collection("Posts").document(postID)
                        .get().addOnSuccessListener(documentSnapshot -> {
                            lovesCount = documentSnapshot.getString("LovesCount");
                            if (lovesCount == null) {
                                flag = 1;
                            } else {
                                flag = Integer.parseInt(lovesCount) + 1;
                            }
                            db.collection("Posts").document(postID)
                                    .update("lovesCount", flag)
                                    .addOnSuccessListener(aVoid1 -> onDestroy());
                        }
                ));


    }

    private void unlike(String userID, String postID) {
        UsersClass usersClass = new UsersClass();
        usersClass.setUser_ID(curr_user.getCurr_ID());
        HashMap<String, Object> map = new HashMap();
        map.put(postID, FieldValue.delete());
        WriteBatch batch = db.batch();
        batch.update(db.collection("Users").document(userID)
                .collection("Notifications").document(postID), map);
        batch.delete(db.collection("Users").document(userID)
                .collection("Notifications").document(postID));
        batch.commit().addOnCompleteListener(task -> {
            if(task.isSuccessful()){
                db.collection("Posts").document(postID)
                        .get().addOnSuccessListener(documentSnapshot -> {
                            lovesCount = documentSnapshot.getString("LovesCount");
                            if (lovesCount == null) {
                                flag = 0; } else {
                                flag = Integer.parseInt(lovesCount) - 1; }

                            db.collection("Posts").document(postID)
                                    .update("lovesCount", flag)
                                    .addOnSuccessListener(aVoid3 -> onDestroy()); });
            }else{
                Log.w(TAG, "LIKESERVICE:failure", task.getException());
                db.collection("Posts").document(postID)
                        .get().addOnSuccessListener(documentSnapshot -> {
                    lovesCount = documentSnapshot.getString("LovesCount");
                    if (lovesCount == null) {
                        flag = 0; } else {
                        flag = Integer.parseInt(lovesCount) - 1; }

                    db.collection("Posts").document(postID)
                            .update("lovesCount", flag)
                            .addOnSuccessListener(aVoid3 -> onDestroy());
                });
            }
        });


    }



}
