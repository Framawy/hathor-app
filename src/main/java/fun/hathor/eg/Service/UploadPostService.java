package fun.hathor.eg.Service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.IBinder;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

import fun.hathor.eg.Classes.GetUriRealPath;
import fun.hathor.eg.Classes.ImageCompress.RealPathUtil;
import fun.hathor.eg.R;
import fun.hathor.eg.Classes.Curr_User;
import fun.hathor.eg.Classes.PostClass;


public class UploadPostService extends Service {
    private StorageReference ref;
    private Uri downloadUri;
    private Curr_User curr_user;
    private FirebaseFirestore database = FirebaseFirestore.getInstance();
    private String img, txt;
    private PostClass post;
    private ArrayList<String> idlist = new ArrayList<>();
    private String postsID;
    private String CHANNEL_ID = "12";
    private Date time;
    private int notificationId = 1;
    private int notificationDoneID = 2;
    private NotificationCompat.Builder builder;
    private NotificationManagerCompat notificationManager;
    private NotificationChannel channel;
    private HashMap<String, Object> postsMap = new HashMap<>();


    public UploadPostService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        curr_user = new Curr_User();
        img = intent.getStringExtra("IMG_URI");
        txt = intent.getStringExtra("POST_TXT");
        notification();
        imageUploader();
        return START_STICKY;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        notificationManager.cancel(notificationId);
    }





    private void imageUploader() {
        ref = FirebaseStorage.getInstance().getReference()
                .child("images/" + curr_user.getCurr_ID() + "/posts_image_" + System.currentTimeMillis());

        String s = compressImage(img);

        ref.putFile(Uri.fromFile(new File(s))).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                ref.getDownloadUrl().addOnSuccessListener(uri -> {
                    downloadUri = uri;
                    postsID = database.collection("Posts").document().getId();
                    post = new PostClass(curr_user.getCurr_ID(), curr_user.getCurr_Displayname(), curr_user.getCurr_ImageUrl().toString(), postsID, downloadUri.toString(), txt);
                    post.setPostID(postsID);
                    writeNewPost(post);
                });
            } else err();
        });
    }


    private void writeNewPost(PostClass post) {
        ///////Trending here
        builder.setProgress(100, 50 , false);
        notificationManager.notify(notificationId, builder.build());

        database.collection("Posts").document(post.getPostID()).set(post).addOnCompleteListener(task1 -> {
            if (task1.isSuccessful()) {
                database.collection("Posts").document(postsID).get().addOnSuccessListener(documentSnapshot -> {
                    time =  documentSnapshot.getDate("time");

                    postsMap.put("postID" , postsID);
                    postsMap.put("time" , time);

                    database.collection("Users").document(post.getUserID())
                        .collection("Posts").document(postsID).set(postsMap);

                database.collection("Users").document(post.getUserID())
                        .collection("Timeline").document(postsID).set(postsMap).addOnSuccessListener(aVoid ->
                        Toast.makeText(this, "Done", Toast.LENGTH_SHORT).show());

                sendPosts();
                });

            } else {
                err();
            }
        });

    }


    private void sendPosts() {
        builder.setProgress(100, 75 , false);
        notificationManager.notify(notificationId, builder.build());

        database.collection("Users").document(curr_user.getCurr_ID()).collection("Followers")
                .get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                for (QueryDocumentSnapshot document : Objects.requireNonNull(task.getResult())) {
                    idlist.add(document.getId());
                }
                for (String s : idlist) {
                    database.collection("Users").document(s).collection("Timeline").document(postsID).set(postsMap);
                }
                Toast.makeText(this, "Post Uploaded", Toast.LENGTH_LONG).show();
                onDestroy();
            } else {
                err();
            }
        });
    }

    private void notification() {
        builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_hathor_notify)
                .setContentText("Uploading post")
                .setChannelId(CHANNEL_ID)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setProgress(100, 25, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);
        }
        notificationManager = NotificationManagerCompat.from(this);
        notificationManager.createNotificationChannel(channel);
        notificationManager.notify(notificationId, builder.build());
    }

    private void err() {
        NotificationCompat.Builder builderDone = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_hathor_notify)
                .setContentText("Something went wrong please try again!")
                .setChannelId(CHANNEL_ID)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            channel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_HIGH);
        }
        notificationManager = NotificationManagerCompat.from(this);
        notificationManager.createNotificationChannel(channel);
        notificationManager.notify(notificationDoneID, builderDone.build());

        onDestroy();
    }



    public String compressImage(String imageUri) {

       // String filePath = getRealPathFromURI(imageUri);
        GetUriRealPath getUriRealPath = new GetUriRealPath();
        //String filePath = getUriRealPath.GetUriRealPath(this , Uri.parse(imageUri));
        String filePath = RealPathUtil.getRealPath(this , Uri.parse(imageUri));
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();

//      by setting this field as true, the actual bitmap pixels are not loaded in the memory. Just the bounds are loaded. If
//      you try the use the bitmap here, you will get null.
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;

//      max Height and width values of the compressed image is taken as 816x612
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

//      width and height values are set maintaining the aspect ratio of the image
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;
            }
        }

//      setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);

//      inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;

//      this options allow android to claim the bitmap memory if it runs low on memory
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
//          load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

//      check the rotation of the image and display it properly
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                    scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix,
                    true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);

//          write the compressed bitmap at the destination specified by filename.
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    private String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int index = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(index);
        }
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }



}
